package enrique.com.helloworldkotlin;

/**
 * Created by enriquelopezmanas on 10/03/2017.
 */

public class Human {
	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
