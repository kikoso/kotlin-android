package enrique.com.helloworldkotlin

import android.app.Fragment
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Toast

import org.jetbrains.anko.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
      //  setContentView(R.layout.activity_main)
        var human = Human()
       // message.text = "Hello Kotlin"

       /* verticalLayout {
            var title = editText {
                id = R.id.todo_title
                hintResource = R.string.title_hint
            }
            button {
                textResource = R.string.add_todo
                onClick { view -> {
                    // do something here
                    title.text = "Foo"
                }
                }
            }
        }*/
        verticalLayout {
            val name = editText()
            button("Say Hello") {
                onClick { toast("Hello, ${name.text}!") }
            }
        }

    }

    fun Fragment.toast(message: CharSequence, duration: Int = Toast.LENGTH_SHORT) {
        Toast.makeText(activity,message,duration).show()
    }
}
