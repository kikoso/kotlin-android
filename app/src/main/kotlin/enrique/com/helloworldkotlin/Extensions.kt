package enrique.com.helloworldkotlin

import android.app.Fragment
import android.widget.Toast

/**
 * Created by enriquelopezmanas on 10/03/2017.
 */

fun Fragment.toast(message: CharSequence, duration: Int = Toast.LENGTH_SHORT) {
    Toast.makeText(activity,message,duration).show()
}

/*view.setOnClickListener{
    toast("HelloWorld")
}:*/